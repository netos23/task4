package com.company;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);

        System.out.println("Введите число к: ");
        int input = in.nextInt();
        System.out.printf("k-ая цыфра: %d", getNumFromNaturalSeries(input));
    }

    /**
     * Решение 0: олимпиадное
     *
     * Алгоритм:
     *      1) k < 10 тогда вернуть k, иначе продолжить
     *
     *      2) Определяем порядок числа
     *          Например: для  k = 11 порядок равен i = 2 так как искомая цыфра в числе 10,
     *          а оно двузначное
     *
     *      3) Вычитаем сумму цыфр предыдущих порядков, которая вычисляется по формуле Sum( i * 10 ^ (i-1) ), из k
     *
     *      4) Цыфры нумируются по следущему принципу:
     *          Например для i = 3
     *          Номер: 1 2 3  4 5 6  7 ...
     *          Цыфра: 1 0 0  1 0 1  1
     *          %i:    1 2 0  1 2 0  1
     *
     *      5) Формируем три группы по остаткам: 0, 1, остальные
     *          0 - последняя цыфра в числе
     *          1 - первая
     *          остальное - цыфра от 2 до n - 1, считая с одного для i > 2
     *
     *      6) В зависимости от группы формируем результат
     *
     * */

    public static int getNumFromNaturalSeries(int k) {
        // Обрабатываем частный случай для 1 - 9
        if (k < 10) {
            return k;
        }

        int i = 1;
        int tenCount = 1;
        int tmp = k;

        //  Определяем порядок числа в котором ноходиться искомая цыфра
        //  и вычитаем сумму цыфр предыдущего порядка
        while (tmp > 0) {
            tmp -= 9 * i * tenCount;
            if (tmp > 0) {
                tenCount *= 10;
                i++;
            }
        }

        // Формируем группы по остаткам
        k = tmp + 9 * i * tenCount;
        int group = k % i;

        // В зваисимости от группы возвращем цыфру
        switch (group) {
            case 0:
                int res = k / i % 10 - 1;
                res = (res == -1) ? 9 : res;

                return res;
            case 1:
                return k / tenCount / i + 1;
            default:
                for (int j = 0; j < group - 1; j++) {
                    tenCount /= 10;
                }

                return k / tenCount / i % 10;
        }

    }


    /**
     * Решение 1: класическоое
     *
     * Тупо брутфорсим
     * Для итерации слева на право подсчитываем какая цыфра в числе искомая и затем достаем ее
     *
     * */
    public static int getNumFromNaturalSeriesClassic(int k) {
        if(k < 10){
            return k;
        }

        for (int i = 1, count = 0; ; i++) {
            int tmp = i;
            int j = 0;
            while (tmp > 0) {

                count++;
                j++;

                if (count == k) {

                    int len = (int) Math.floor( Math.log10(i) ) + 1;
                    int divider = (int) Math.pow(10, len - j);

                    return i / divider % 10;
                } else {
                    tmp /= 10;
                }
            }

        }
    }

    /**
     * Решение 2: запрещенное
     *
     * Опять брутфорс
     * Работает всегда, но медлено.
     * Используется для тестов первых двух
     *
     * */
    public static int getNumFromNaturalSeriesString(int k) {
        for (int i = 1, count = 0; ; i++) {
            for (char ch : (i + "").toCharArray()) {
                count++;
                if (count == k) {
                    return Integer.parseInt("" + ch);
                }
            }
        }
    }


    /**
     *
     * Тест для быстрого решения
     *
     * */
    private static void testFast(int count) {
        for (int i = 1; i < count; i++) {

            if (getNumFromNaturalSeries(i) != getNumFromNaturalSeriesString(i)) {
                System.out.println("wrong " + i);
            } else {
                System.out.println("ok");
            }
        }
    }

    /**
     *
     * Тест для классического решения
     *
     * */
    private static void  testClassic(int count){
        for (int i = 1; i < count; i++) {

            if (getNumFromNaturalSeriesClassic(i) != getNumFromNaturalSeriesString(i)) {
                System.out.println("wrong " + i);
            } else {
                System.out.println("ok");
            }
        }
    }

    /**
     *
     * Выводит нумерацию цыфр для n-го порядка
     *
     * */
    private static void debugPrint(int n) {
        int count = 1;
        int start = (int) Math.pow(10, n);
        double thresholdValue = Math.pow(10, n + 1);

        for (int i = start; i < thresholdValue; i++) {
            for (char ch : (i + "").toCharArray()) {
                System.out.printf("ch: %c k1: %d group: %d\n", ch, count, count % 2);
                count++;
            }
            System.out.println();
        }
    }
}
